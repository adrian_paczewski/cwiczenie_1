package com.example.zadanie1;

import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class Quiz extends Fragment {

    private static final int QUESTIONS = 6;
    private static final int ANSWERS = 4;
    private List<String> fileNameList;
    private List<String> quizAnswersList;
    private List<Question> questions;
    private String correctAnswer;
    private int totalGuesses;
    private int correctAnswers;

    private TextView questionNumberTextView;
    private ImageView imageView;
    private TextView answerTextView;
    private TextView questionTextView;

    private Button[] answers;

    private View.OnClickListener guessButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Button guessButton = ((Button) v);
            String guess = guessButton.getText().toString();
            String answer = correctAnswer;
            ++totalGuesses;

            if (guess.equals(answer)) {
                ++correctAnswers;

                answerTextView.setText(answer);

                disableButtons();

                if (correctAnswers == QUESTIONS) {
                    answerTextView.setText(getString(R.string.results,
                            totalGuesses,
                            (correctAnswers * (100 / (double) totalGuesses))));
                } else {
                    try {
                        Thread.sleep(250);
                        loadNextQuestion();
                    } catch (InterruptedException e) {
                        Log.e(this.getClass().getName(), e.getMessage());
                    }

                }
            } else {
                answerTextView.setText(R.string.incorrect_answer);
                guessButton.setEnabled(false);
            }
        }

    };

    public Quiz() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment CoronaQuiz.
     */
    public static Quiz newInstance() {
        return new Quiz();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_quiz, container, false);

        initializeFields();
        setViews(view);
        questionNumberTextView.setText(getString(R.string.question, 1, QUESTIONS));

        resetQuiz();
        return view;
    }

    private void initializeFields() {
        fileNameList = new ArrayList<>();
        quizAnswersList = new ArrayList<>();
        questions = CSVReader.getQuestions(getResources().openRawResource(R.raw.quiz));
    }

    private void setViews(View view) {
        imageView = view.findViewById(R.id.flagImageView);
        answerTextView = view.findViewById(R.id.answerTextView);
        questionTextView = view.findViewById(R.id.questionTextView);
        questionNumberTextView = view.findViewById(R.id.questionNumberTextView);

        answers = new Button[ANSWERS];
        answers[0] = view.findViewById(R.id.button1);
        answers[1] = view.findViewById(R.id.button2);
        answers[2] = view.findViewById(R.id.button3);
        answers[3] = view.findViewById(R.id.button4);

        for (Button button : answers) {
            button.setOnClickListener(guessButtonListener);
        }
    }

    public void resetQuiz() {

        loadFiles();

        correctAnswers = 0;
        totalGuesses = 0;
        quizAnswersList.clear();

        int counter = 1;

        while (counter <= QUESTIONS) {
            String filename = fileNameList.get(counter - 1);
            quizAnswersList.add(filename);
            ++counter;
        }

        loadNextQuestion();
    }

    private void loadFiles() {
        AssetManager assets = getActivity().getAssets();

        fileNameList.clear();

        try {
            for (String path : Objects.requireNonNull(assets.list("quiz")))

                fileNameList.add(path.replace(".png", ""));
        } catch (IOException exception) {
            Log.e(this.getClass().getName(), "Error loading image file names", exception);
        }
    }

    private void loadNextQuestion() {

        String nextImage = quizAnswersList.remove(0);
        correctAnswer = nextImage;
        answerTextView.setText("");


        questionNumberTextView.setText(getString(R.string.question, (correctAnswers + 1), QUESTIONS));

        int questionNo = Integer.parseInt(nextImage.substring(nextImage.lastIndexOf("_") + 1));
        Question question = questions.get(questionNo - 1);

        questionTextView.setText(question.getQuestion());

        int i = 0;
        for (Map.Entry<String, Boolean> answer : question.getAnswers().entrySet()) {
            answers[i].setEnabled(true);
            answers[i].setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            answers[i].setText(answer.getKey());
            if (answer.getValue()) {
                correctAnswer = answer.getKey();
            }
            i++;
        }

        AssetManager assets = getActivity().getAssets();

        try (InputStream stream =
                     assets.open("quiz" + "/" + nextImage + ".png")) {
            Drawable flag = Drawable.createFromStream(stream, nextImage);
            imageView.setImageDrawable(flag);

        } catch (IOException exception) {
            Log.e(this.getClass().getName(), "Error loading " + nextImage, exception);
        }

    }


    private void disableButtons() {
        for (Button button : answers) {
            button.setEnabled(false);
        }
    }
}
