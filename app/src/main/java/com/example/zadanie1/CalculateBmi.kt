package com.example.zadanie1

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_bmi.view.*

class CalculateBmi : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bmi, container, false)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment CalculateBMI.
         */

        @JvmStatic
        fun newInstance() =
                CalculateBmi()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setCalculateButton(view)
    }

    private fun setCalculateButton(view: View) {
        view.button_first.setOnClickListener {
            val calculated: Double = calculate(view.weightInput.text.toString().toDouble(),
                    view.heightInput.text.toString().toDouble())
            view.result.setText(calculated.toString());
        }
    }

    private fun calculate(mass: Double, height: Double): Double {
        val heightMeters: Double = height / 100
        return mass / (heightMeters * heightMeters)
    }
}